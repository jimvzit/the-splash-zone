![splash zone header](splash_zone_header_2000.png)


# The Splash Zone


The Splash Zone is a collection of plymouth splash animations, not only of sight and sound, but of mind.

<br>

# Available Splash Animations

<br>

<table>
<tr>
<td>
<p><h4>fe-pulse</h4></p>
<p>The fe-pulse boot animation displays a fedora logo with the center "f" pulsing from dark to light. Fe-pulse was created by modifying the spinfinity plymouth theme, replacing the "infinity" animation with the pulsing logo. Like spinfinity, spinf-mod uses the two-step plymouth plugin.</p>
</td>
<td><img src="images/fe-pulse-500.png" width="750px" /></td>
</table>

<br>

<table>
<tr>
<td>
<p><h4>qu-pulse</h4></p>
<p>The qu-pulse boot animation displays the qubes logo icon in the center that pulses from dark to light. Qu-pulse was created by modifying Aditya Shakya's (@adi1090x) abstract_ring plymouth animation. Qu-pulse uses the script plugin and requires a configuration file and script file.</p>
</td>
<td><img src="images/qu-pulse-500.png" width="750px" /></td>
</tr>
</table>

<br>

<table>
<tr>
<td>
<p><h4>spinf-mod</h4></p>
<p>Spinf-mod is a plymouth boot animation based on the spinfinity theme. Spinf-mod displays the 3 color fedora logo, a gradient background and the "infinity" animation has been colorized to blue. Spinf-mod uses the two-step plymouth plugin and is enabled across more modes (e.g., updates).</p>
</td>
<td><img src="images/spinf-mod-500.png" width="750px" /></td>
</tr>
</table>

<br>

### Installation

To use one of the plymouth boot animations, download the directory (&lt;directory name&gt;) and contents to you local device and then copy to your ../plymouth/themes/directory.

Verify that the directory (&lt;directory name&gt;) has been installed.

```
$ sudo plymouth-set-default-theme -l

bgrt
details
<directory name>
hot-dog
spinfinity
spinner
text
tribar
```

<br>

Change the default plymouth theme to the new theme using the `plymouth-set-default-theme` command, along with the new theme name (i.e., &lt;theme name&gt;) and the `-R` option.


```
$ sudo plymouth-set-default-theme <theme name> -R
$
```

<br>

Verify the default plymouth theme has been changed using the `plymouth-set-default-theme` command. The example below show the default plymouth theme has been updated to &lt;theme name&gt;.

```
$ sudo plymouth-set-default-theme
<theme name>
```

<br>

### About

The Splash Zone project is a work in progress.
